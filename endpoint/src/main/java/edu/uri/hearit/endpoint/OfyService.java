package edu.uri.hearit.endpoint;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import edu.uri.hearit.endpoint.entities.Post;
import edu.uri.hearit.endpoint.entities.Profile;

/**
 * Created by Orrett on 10/7/2014.
 */
public class OfyService {
    static {
        factory().register(Post.class);
        factory().register(Profile.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
