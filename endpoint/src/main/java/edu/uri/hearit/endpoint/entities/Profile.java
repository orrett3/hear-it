package edu.uri.hearit.endpoint.entities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

import java.util.ArrayList;

/**
 * Created by Orrett on 10/16/2014.
 */
@Entity
@Cache
public class Profile {
    @Id private String Id;

    @Index private String username;

    private String Bio;

    private String profilePic;

    private boolean firstLogin;

    @Load private ArrayList<Ref<Post>> posts = new ArrayList<Ref<Post>>();

    @Load private ArrayList<Ref<Profile>> friends = new ArrayList<Ref<Profile>>();
/*------------------------------------------------------------------------------------*/
    public Profile(String Id, boolean firstLogin){
        this.Id = Id;
        this.firstLogin = firstLogin;
    }

    public ArrayList<Ref<Profile>> getFriends() {
        return friends;
    }

    public void addFriend(Ref<Profile> friend) {
        this.friends.add(friend);
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBio() {
        return Bio;
    }

    public void setBio(String bio) {
        Bio = bio;
    }

    public ArrayList<Ref<Post>> getPosts() {
        return posts;
    }

    public void addPost(Ref<Post> post) {
        posts.add(post);
    }

    public boolean isFirstLogin(){
        return firstLogin;
    }



}
