package edu.uri.hearit.endpoint.entities.bio;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by Orrett on 10/6/2014.
 */
@Entity
public class Education {

    @Id long key;
    @Index private int yearStart;
    @Index private int yearEnd;
    @Index private String institutionName;

    public int getYearStart() {
        return yearStart;
    }

    public void setYearStart(int yearStart) {
        this.yearStart = yearStart;
    }

    public int getYearEnd() {
        return yearEnd;
    }

    public void setYearEnd(int yearEnd) {
        this.yearEnd = yearEnd;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String name) {
        this.institutionName = name;
    }
}
