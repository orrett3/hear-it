package edu.uri.hearit.endpoint.entities;

import com.google.appengine.api.users.User;
import com.googlecode.objectify.annotation.Entity;

/**
 * Created by Orrett on 10/16/2014.
 */
@Entity
public class Session {
    final private User user;

    final private Profile profile;


    public Session(User user, Profile profile) {
        this.user = user;
        this.profile = profile;
    }

    public User getUser(){
        return user;
    }

    public Profile getProfile(){
        return profile;
    }

}
