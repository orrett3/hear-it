/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package edu.uri.hearit.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Work;

import javax.inject.Named;


import edu.uri.hearit.endpoint.entities.Post;
import edu.uri.hearit.endpoint.entities.Profile;
import edu.uri.hearit.endpoint.entities.Session;
import javafx.geometry.Pos;

import static edu.uri.hearit.endpoint.OfyService.ofy;

/** An endpoint class we are exposing */
@Api(name = "hearItApi", version = "v1", namespace = @ApiNamespace(ownerDomain = "endpoint.hearit.uri.edu", ownerName = "endpoint.hearit.uri.edu", packagePath=""))
public class MyEndpoint {

    @ApiMethod(name = "authenticate")
    public Session auth(final User user) throws OAuthRequestException{
        Profile userProfile;
        Session userSession;

        if(user == null){
            throw  new OAuthRequestException(AppEngineConstants.OAUTH_EXCEPTION_STRING);
        }else{
            userProfile = ofy().transact(new Work<Profile>() {
                @Override
                public Profile run() {
                    //create the DB key from logged in user ID
                    Key<Profile> mKey = Key.create(Profile.class, user.getUserId());
                    //Load the profile from the DB
                    Profile mProfile = ofy().load().key(mKey).now();
                    //If profile is null, create one and store it in the database
                    if(mProfile == null){
                        mProfile = createProfile(user);
                        ofy().save().entity(mProfile);
                    }
                    return mProfile;
                }
            });

            userSession = new Session(user, userProfile);
        }

        return userSession;
    }

    private Profile createProfile(User user){
        Profile p = new Profile(user.getUserId(), true);
        return p;
    }

    private Session postClip(User user, final Session session, final Post post) throws OAuthRequestException{
        if(user == null){
            throw new OAuthRequestException(AppEngineConstants.OAUTH_EXCEPTION_STRING);
        }else {
            Ref<Post> reference = ofy().transact(new Work<Ref<Post>>() {
                @Override
                public Ref<Post> run() {
                    //Save the post to the database
                    Ref<Post> temp = Ref.create(ofy().save().entity(post).now());
                    //Get current profile & create a key
                    Key<Profile> profileKey = Key.create(Profile.class,
                            session.getProfile().getId());
                    //Load profile, add post to profile, save it
                    Profile tempProfile = ofy().load().key(profileKey).now();
                    tempProfile.addPost(temp);
                    ofy().save().entity(tempProfile);

                    return temp;
                }
            });

            session.getProfile().addPost(reference);
            //@todo upload the file and create a link to that file
            return session;
        }
    }

    private Post getPost(User user, Session session, Ref postRef) throws OAuthRequestException{
        if(user == null){
            throw new OAuthRequestException(AppEngineConstants.OAUTH_EXCEPTION_STRING);
        }else {
            return (Post)ofy().load().key(postRef.getKey()).now();
        }
    }
    @ApiMethod(name = "post")
    public Ref<Post> post(User user, final Post userPost){
        return null;
    }

    /** A simple endpoint method that takes a name and says Hi back */
    @ApiMethod(name = "sayHi")
    public MyBean sayHi(@Named("name") String name) {
        MyBean response = new MyBean();
        response.setData("Hi, " + name);

        return response;
    }

}
