package edu.uri.hearit.endpoint;

/**
 * Created by Orrett on 10/28/2014.
 */
public class AppEngineConstants {

    public static final String OAUTH_EXCEPTION_STRING = "User is invalid, please sign in with " +
            "correct credentials";
}
